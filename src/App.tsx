import React, { Component } from 'react'
import './App.css'
import radovanWantsYou from './assets/radovanwantsyou.png'

class App extends Component {
    render(): React.ReactElement {
        return (
            <div className="App">
                <img src={radovanWantsYou} alt={'Join Radovan! Radovan wants you'} />
            </div>
        )
    }
}

export default App
