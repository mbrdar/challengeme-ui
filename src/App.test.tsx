import React from 'react'
import App from './App'
import { createRenderer } from 'react-test-renderer/shallow'

test('renders learn react link', () => {
    const shallowRenderer = createRenderer()
    shallowRenderer.render(<App />)
    expect(shallowRenderer.getRenderOutput()).toMatchSnapshot()
})
